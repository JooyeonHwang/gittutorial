# README #

### 이 저장소의 용도는? ###

* 이 저장소는 git 기본 커맨드를 학습하기 위해 사용 됩니다.
* 아무나 자유롭게 clone, push, pull 해보시기 바랍니다. 

### 기본 커맨드 수행 해보기 ###

* git clone 해보기
* git 저장소 main브랜치의 최신 커밋의 내용으로 pull 해보기
* new branch 해보기
    * 브랜치명은 자신의 이름 이니셜로 지정
    * 예 : jyhwang
* profile.txt 파일을 생성하여 다음과 같이 자신의 이름과 이메일 주소 기입하기
```
Name : Jooyeon Hwang
Email : jyhwang@4dreplay.com
```
* add 또는 drag&drop으로 profile.txt staging 해보기
* staged file을 Commit 해보기
    * log에는 다음과 같이 해당 커밋 내용을 1줄로 파악할 수 있도록 설명
    ```
    Added profile.txt
    ```
* push 하여 서버 저장소에 나의 수정사항 반영시키기
    * 좌측에 "Push?" 체크박스를 선택하고 Push 한다.

### 임시 커밋 작성 후 reset 해보기 ###

* profile.txt 를 열어 다음과 같이 작성
```
Name: Jooyeon Hwang
Email : jyhwang@4dreplay.com
Phone : Todo
```
* stage 후 Commit
    * commit log는 "Temporary commit" 이런 식으로 작성
* Reset 커맨드를 이용하여 이전 커밋으로 복귀해보기
    * 이전 커밋에서 오른쪽 버튼 클릭
    * Reset current branch to this commit (Mixed)
* profile.txt 파일이 modified 상태로 되어 있으면 성공!

### reverse commit 만들어 보기 ###

* profile.txt 를 열어 다음과 같이 작성
```
Name: Jooyeon Hwang
Email : jyhwang@4dreplay.com
Phone : XXX-XXXX-XXXX
```
* stage 후 Commit
    * commit log는 "added undefined phone number"
* Reverse commit을 쌓아보기
    * 이전 커밋에서 오른쪽 버튼 클릭
    * Reverse commit...
* 다음과 같은 log의 커밋이 새로 생성되었으면 성공!
    * Revert "added undefined phone number"
* 다음 과제로 넘어가기 전에 현재까지 내용을 push하여 서버에 반영한다.

### 내 브랜치를 main 브랜치에 merge 해보기 ###

* main 브랜치를 더블클릭하여 checkout 하기
    * 좌측 브랜치에서 main 좌측에 동그라미가 있으면 성공!
* 자신의 이름의 브랜치에서 오른쪽 버튼을 눌러 merge 해보기
    * Are you sure you want to merge "yourname" into your current branch?
        * 체크박스 체크 : Create a new commit even if fast-forward is possible 
    * Merge branch commit이 생성되었으면 성공!
    * push는 하지 마세요~

### 문의사항 ###

* jyhwang@4dreplay.com (Jooyeon Hwang)

### Bitbucket - Asana sync test ###

* modified this file.
